using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Recollectable", menuName = "ScriptableObjects/Recollectable", order = 1)]
public class RecollectableSO : ScriptableObject
{
    public GameObject model;
    public GameObject modelObj;
    public GameObject prefab;
    public Material materialBase;
    public Material materialHighLight;
    public Sprite sprite;
}
