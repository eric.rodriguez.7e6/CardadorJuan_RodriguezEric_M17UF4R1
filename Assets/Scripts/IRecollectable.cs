using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRecollectable
{
    public GameObject Recollect();
}
