using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconTrack : MonoBehaviour
{
    public GameObject target;

    private void Start()
    {
        if(target != null )
        {
            
            GetComponent<SpriteRenderer>().sprite = target.CompareTag("Player") ? target.GetComponent<PlayerController>().icon : target.GetComponent<Recollectable>().recSO.sprite;
        }
        if(target.name == "Anvorgesa") transform.localScale = new Vector3(20,20,20);
    }
    private void Update()
    {
        if (target != null)
        {
            transform.position = new Vector3(target.transform.position.x, 600, target.transform.position.z);
            if (target.CompareTag("Player")) transform.rotation = Quaternion.Euler(90, target.transform.eulerAngles.y + 90, 0);
        }
        else Destroy(gameObject);
    }
}
