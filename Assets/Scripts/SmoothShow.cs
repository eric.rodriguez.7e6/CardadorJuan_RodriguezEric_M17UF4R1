using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SmoothShow : MonoBehaviour
{
    private bool start;
    private TextMeshProUGUI text;
    private Image img;
    void Start()
    {
        img = GetComponent<Image>();
        text = GetComponentInChildren<TextMeshProUGUI>();
    }

    void Update()
    {
        if (start)
        {
            if (img.color.a < 1) img.color = new(img.color.r,img.color.g, img.color.b, img.color.a + 0.01f);
            if (text.color.a < 1) text.color = new(text.color.r, img.color.g, text.color.b, text.color.a + 0.01f);
        }
    }
    public void Show()
    {
        start = true;
    }
}
