using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recollectable : MonoBehaviour, IRecollectable, IInteractive
{
    public RecollectableSO recSO;
    public GameObject iconPrefab;
    private bool recollected;
    private bool highlighted;
    private float clock;

    private MeshRenderer mr;

    private void Start()
    {
        recSO.modelObj = Instantiate(recSO.model, transform);
        recollected = false;
        clock = Time.time;
        mr = GetComponentInChildren<MeshRenderer>();
        if (PlayerPrefs.HasKey(name)) GameObject.Find("Dreyar").GetComponent<PlayerController>().PickUpAdmin(gameObject, PlayerPrefs.GetInt(name));
        GameObject icon = Instantiate(iconPrefab);
        icon.GetComponent<IconTrack>().target = gameObject;
    }

    private void Update()
    {
        if (Time.time - clock > 0.1f)
        {
            if (recollected) Destroy(gameObject);

            mr.material = highlighted ? recSO.materialHighLight : recSO.materialBase;

            highlighted = false;

            clock = Time.time;
        }
    }
    public GameObject Recollect()
    {
        recollected = true;
        return recSO.prefab;
    }
    public void Highlight()
    {
        highlighted = true;
    }
    public void Interact(PlayerController player)
    {
        // Nothing
    }
}
