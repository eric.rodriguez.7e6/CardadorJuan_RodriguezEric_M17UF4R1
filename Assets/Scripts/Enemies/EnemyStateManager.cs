using System.Collections;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStateManager : MonoBehaviour
{
    public EnemyStateBase state;
    public NavMeshAgent agent;

    public EnemyPatrol patrolState = new();
    public EnemyFollow followState = new();
    public EnemyAttack attackState = new();
    [Range(1, 20)]
    public float attackRange;
    [Range(1, 50)]
    public float detectionRange;
    [Range(1, 50)]
    public float offsetDetectionRange;
    //private bool disponible;
    public Rigidbody rb;
    public Animator anim;
    [Range(0, 20)]
    public float speed;
    public List<Vector2> patrolPoints;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        //disponible = true;
        SwitchState(patrolState);
    }
    private void Update()
    {
        state.UpdateState(this);
    }
    public void SwitchState(EnemyStateBase newState)
    {
        state?.ExitState(this);
        state = newState;
        state.EnterState(this);
    }
    //IEnumerator CD()
    //{
    //    disponible = false;
    //    yield return new WaitForSeconds(.1f);
    //    disponible = true;
    //}
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(255, 255, 255, .3f);
        Gizmos.DrawSphere(transform.position + transform.forward * offsetDetectionRange, detectionRange);
    }
}
