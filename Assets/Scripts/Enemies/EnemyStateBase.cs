using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public abstract class EnemyStateBase
{
    public abstract void EnterState(EnemyStateManager enemy);
    public abstract void UpdateState(EnemyStateManager enemy);
    public abstract void ExitState(EnemyStateManager enemy);
}

public class EnemyPatrol : EnemyStateBase
{
    private int patrolIndex;
    public override void EnterState(EnemyStateManager enemy)
    {
        enemy.anim.SetBool("Following", false);
        //Debug.Log("Descanso");
        enemy.agent.speed = enemy.speed;
    }
    public override void UpdateState(EnemyStateManager enemy)
    {
        if (Physics.OverlapSphere(enemy.transform.position + enemy.transform.forward * enemy.offsetDetectionRange, enemy.detectionRange).Any(item => item.CompareTag("Player")))
        {
            enemy.SwitchState(enemy.followState);
            return;
        }

        //enemy.agent.SetDestination(new(500,0,600));
        enemy.agent.SetDestination(new(enemy.patrolPoints[patrolIndex].x, 0, enemy.patrolPoints[patrolIndex].y));
        if (Vector2.Distance(new(enemy.transform.position.x,enemy.transform.position.z), enemy.patrolPoints[patrolIndex]) < 2) patrolIndex = patrolIndex + 1 >= enemy.patrolPoints.Count ? 0 : patrolIndex + 1;
    }
    public override void ExitState(EnemyStateManager enemy)
    {
        Debug.Log("Dejo de descansar");
    }
}

public class EnemyFollow : EnemyStateBase
{
    public override void EnterState(EnemyStateManager enemy)
    {
        enemy.anim.SetBool("Following", true);
        //Debug.Log("Persiguiendo");
        enemy.agent.speed = enemy.speed * 2;
    }
    public override void UpdateState(EnemyStateManager enemy)
    {
        Collider[] clls = Physics.OverlapSphere(enemy.transform.position + enemy.transform.forward * enemy.offsetDetectionRange , enemy.detectionRange).Where(item => item.CompareTag("Player")).ToArray();
        if (clls.Length == 0)
        {
            enemy.SwitchState(enemy.patrolState);
            return;
        }
        enemy.agent.SetDestination(clls[0].transform.position);
        //enemy.rb.velocity = (clls[0].transform.position - enemy.transform.position).normalized * enemy.speed;
        enemy.transform.LookAt(new Vector3(clls[0].transform.position.x,enemy.transform.position.y,clls[0].transform.position.z));

        if (Vector3.Distance(enemy.transform.position, clls[0].transform.position) <= enemy.attackRange)
        {
            enemy.SwitchState(enemy.attackState);
            return;
        }

    }
    public override void ExitState(EnemyStateManager enemy)
    {
        //Debug.Log("Dejo de estar alertado");
    }
}

public class EnemyAttack : EnemyStateBase
{
    bool hitted = false;
    public override void EnterState(EnemyStateManager enemy)
    {
        enemy.anim.SetTrigger("Attack");
        //Debug.Log("Atacando");

        enemy.agent.ResetPath();
        enemy.agent.isStopped = true;
        enemy.agent.speed = 0;
        enemy.rb.velocity = Vector3.zero;
    }
    public override void UpdateState(EnemyStateManager enemy)
    {
        Collider[] clls = Physics.OverlapSphere(enemy.transform.position + enemy.transform.forward * enemy.offsetDetectionRange, enemy.detectionRange).Where(item => item.CompareTag("Player")).ToArray();
        
        if (clls.Length == 0)
        {
            enemy.SwitchState(enemy.patrolState);
            return;
        }
        if (!hitted && clls.Length > 0)
        {
            hitted = true;
            clls[0].GetComponent<PlayerController>().Hit(enemy.transform.position);
        }
        if (Vector3.Distance(enemy.transform.position, clls[0].transform.position) > enemy.attackRange + 1)
        {
            enemy.SwitchState(enemy.patrolState);
            return;
        }

    }
    public override void ExitState(EnemyStateManager enemy)
    {
        hitted = false;
        enemy.agent.isStopped = false;
        //Debug.Log("Fuera de rango de ataque");
    }
}
