using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoint : MonoBehaviour, IInteractive
{
    private bool highlighted;
    private float clock;

    private MeshRenderer mr;

    private void Start()
    {
        clock = Time.time;
        mr = GetComponentInChildren<MeshRenderer>();
    }

    private void Update()
    {
        if (Time.time - clock > 0.1f)
        {

            if (highlighted) mr.material.EnableKeyword("_EMISSION");
            else mr.material.DisableKeyword("_EMISSION");

            highlighted = false;

            clock = Time.time;
        }
    }
    public void Highlight()
    {
        highlighted = true;
    }
    public void Interact(PlayerController player)
    {
        player.Save();
        Debug.Log("Guardado!");
    }
}
