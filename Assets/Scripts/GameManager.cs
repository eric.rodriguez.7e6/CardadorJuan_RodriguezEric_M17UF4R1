using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] private bool victory;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            SceneManager.LoadScene(0);
        }
        else if (instance != this) Destroy(gameObject);
    }
    private void Start()
    {
        SoundManager.instance.Play("OST");
    }
    public void CheckItems(List<GameObject> inventory, GameObject hud)
    {
        foreach (var item in hud.GetComponentsInChildren<Image>())
        {
            if (inventory.Any(obj => item.name.Contains(obj.name))) item.color = Color.white;
            else item.color = Color.black;
        }

        if (!victory && hud.GetComponentsInChildren<Image>().All(item => item.color == Color.white)) victory = true;
    }
    public static void ShowHideControls()
    {
        RectMask2D rm = GameObject.Find("ControlsMask").GetComponent<RectMask2D>();
        rm.enabled = !rm.enabled;
    }
    public void CheckGoals()
    {
        if (GameObject.Find("Items").GetComponentsInChildren<Image>().All(item => item.color == Color.white))
        {
            GameObject.Find("Final").GetComponent<SmoothShow>().Show();
            StartCoroutine(Win());
            return;
        }
    }
    public static void CloseGame()
    {
        Application.Quit();
    }
    public IEnumerator Win()
    {
        yield return new WaitForSeconds(2);
        CloseGame();
    }
}