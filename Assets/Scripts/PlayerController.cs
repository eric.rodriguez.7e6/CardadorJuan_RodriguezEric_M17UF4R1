using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public float altura;
    private Rigidbody rb;
    public float speed;
    private Animator anim;
    private float clock;
    private Vector2 movement;
    [SerializeField] private float sensitivity = 0.5f;
    [SerializeField] private float jumpForce = 50;
    private bool onGround;
    private float jumpDelay;
    private bool cameraRotationFree;
    private Quaternion cameraRotation;
    private GameObject interactive;
    public List<GameObject> inventory;
    private int interactCD;
    [SerializeField] private List<GameObject> inventorySlots;
    public Sprite icon;
    private Vector3 moveDirection;

    [SerializeField] private GameObject itemsHUD;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        clock = Time.time;
        Cursor.lockState = CursorLockMode.Locked;
        cameraRotationFree = true;
        cameraRotation = Camera.main.transform.transform.localRotation;

        if (PlayerPrefs.HasKey("Position"))
        {
            string[] pos = PlayerPrefs.GetString("Position").Split('/');
            transform.position = new(float.Parse(pos[0]), float.Parse(pos[1]), float.Parse(pos[2]));
        }

        if (PlayerPrefs.HasKey("Rotation"))
        {
            string[] pos = PlayerPrefs.GetString("Rotation").Split('/');
            transform.rotation = new(float.Parse(pos[0]), float.Parse(pos[1]), float.Parse(pos[2]), float.Parse(pos[3]));
        }
    }
    void Update()
    {
        GameManager.instance.CheckItems(inventory, itemsHUD);
        CameraController();
        LookForward();

        // onGround check
        onGround = Physics.Raycast(transform.position + Vector3.up * .1f, Vector3.down, 0.11f);
        anim.SetBool("OnGround", onGround);

        // Jump System
        if (jumpDelay < 13 && anim.GetFloat("Attacking") == 0)
        {
            rb.drag = 5;
            Physics.Raycast(transform.position + Vector3.up * .1f, Vector3.down, out RaycastHit hit, 20);

            moveDirection = transform.forward * movement.y + transform.right * movement.x;

            moveDirection = Vector3.ProjectOnPlane(moveDirection, hit.normal).normalized;

            Vector3 force = speed * moveDirection
                * (Convert.ToInt32(anim.GetBool("Running")) * Convert.ToInt32(onGround) * 1 + 1)
                * (Convert.ToInt32(anim.GetBool("Crouching")) * Convert.ToInt32(onGround) * -0.3f + 1)
                * (Convert.ToInt32(anim.GetBool("Dance")) * Convert.ToInt32(onGround) * -1f + 1)
                * (Convert.ToInt32(anim.GetBool("Death")) * Convert.ToInt32(onGround) * -1f + 1);
            force = Vector3.Scale(force, new(1,3,1));
            Debug.DrawRay(transform.position + Vector3.up * .1f, rb.velocity, Color.yellow, 15);

            rb.AddForce(force);
        } else rb.velocity = new(0, rb.velocity.y, 0);

        // Clock

        if (Time.time - clock > 0.1f)
        {
            // Gravity Plus
            if (!onGround) rb.velocity -= new Vector3(0, 10f, 0);
            else rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);

            if (anim.GetFloat("Attacking") > 0) anim.SetFloat("Attacking", anim.GetFloat("Attacking") - 1);
            clock = Time.time;
            if(jumpDelay > 12)
            {
                anim.ResetTrigger("Jump");
                jumpDelay--;
                if (jumpDelay == 12) rb.velocity = new Vector3(0, jumpForce, 0);
                SoundManager.instance.Play("Jump");
            }
            else if(jumpDelay < 13 && jumpDelay > 0 && onGround)
            {
                jumpDelay--;
            }

            // PickUp Cooldown
            if(interactCD > 0) interactCD--;
        }

        // Setting animator variables
        anim.SetFloat("Walking", movement.y);
        anim.SetFloat("SideWalk", movement.x);
    }
        
    private void CameraController()
    {
        // Camera Selection by interactions

        // Aimming
        if (anim.GetBool("Aiming") && onGround && anim.GetFloat("Attacking") == 0 && !anim.GetBool("Dance") && !anim.GetBool("Death"))
        {
            if (cameraRotationFree)
            {
                cameraRotation = Camera.main.transform.transform.localRotation;
                cameraRotationFree = false;
            }
            Camera.main.transform.transform.SetLocalPositionAndRotation(
                Vector3.MoveTowards(Camera.main.transform.transform.localPosition, new Vector3(3, 18, -8), 0.1f),
                Quaternion.RotateTowards(Camera.main.transform.transform.localRotation, new Quaternion(0.087f, 0, 0, 0.9f), 1f)
                );
        }
        // Dancing
        else if (onGround && anim.GetBool("Dance") && !anim.GetBool("Death"))
        {
            if (cameraRotationFree)
            {
                cameraRotation = Camera.main.transform.transform.localRotation;
                cameraRotationFree = false;
            }

            Camera.main.transform.transform.SetLocalPositionAndRotation(
                Vector3.MoveTowards(Camera.main.transform.transform.localPosition, new Vector3(0, 15, 30), 0.3f),
                Quaternion.RotateTowards(Camera.main.transform.transform.localRotation, new Quaternion(0, 1, -0.087f, 0), 0.8f)
                );
        }
        // Returning to normal from dancing or aimming
        else if (!cameraRotationFree)
        {
            Camera.main.transform.transform.localRotation = Quaternion.RotateTowards(Camera.main.transform.transform.localRotation, cameraRotation, 1f);
            if (Camera.main.transform.transform.localRotation == cameraRotation) cameraRotationFree = true;
        }

        // Camera rotation by mouse

        if (cameraRotationFree && !anim.GetBool("Death"))
        {
            transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * sensitivity * 10, 0), Space.Self);

            Quaternion newRotation = Quaternion.Euler(-Input.GetAxis("Mouse Y") * sensitivity * 5, 0, 0);
            float a = (cameraRotation * newRotation).x * Mathf.Rad2Deg * 2;

            if (a <= 80 && a >= -20) cameraRotation *= newRotation;

            Camera.main.transform.transform.localRotation = Quaternion.RotateTowards(Camera.main.transform.transform.localRotation, cameraRotation, 10f);

            Vector3 pos = new(
                0,
                25 * Mathf.Sin(cameraRotation.x * Mathf.Rad2Deg * 2 * Mathf.Deg2Rad) + 10,
                -30 * Mathf.Abs(Mathf.Cos(cameraRotation.x * Mathf.Rad2Deg * 2 * Mathf.Deg2Rad))
                );

            Camera.main.transform.transform.localPosition = Vector3.MoveTowards(Camera.main.transform.transform.localPosition, pos, 1f);
        }

        RaycastHit[] rc = Physics.RaycastAll(transform.position, Camera.main.transform.position - transform.position, Vector3.Distance(transform.position, Camera.main.transform.position) - 0.5f).Where(item => item.collider.name != name).ToArray();
        if (rc.Length > 0)Camera.main.transform.transform.position = rc[0].point;
    }
    private void LookForward()
    {
        Vector3 midPoint = transform.position + new Vector3(0,altura,0);
        Debug.DrawRay(midPoint, midPoint - Camera.main.transform.position, Color.red, 0.05f);
        RaycastHit[] rc = Physics.RaycastAll(midPoint, midPoint - Camera.main.transform.position, 30).Where(item => item.collider.name != name).ToArray();

        if (rc.Length > 0)
        {            
            Collider[] clls = Physics.OverlapSphere(rc[0].point, 10).Where(item => item.TryGetComponent(out IInteractive a)).ToArray();

            if (clls.Length > 0)
            {
                clls = clls.OrderBy(item => Vector3.Distance(item.ClosestPoint(rc[0].point), rc[0].point)).ToArray();

                bool canInteract = false;

                foreach (var cll in clls)
                {
                    if(!canInteract)
                    {
                        if(cll.TryGetComponent(out IInteractive iInt))
                        {
                            iInt.Highlight();
                            canInteract = true;
                            interactive = cll.gameObject;
                        }
                    }
                }
                if (!canInteract) interactive = null;
            }
        }
    }
    public void Hit(Vector3 enemy)
    {
        Debug.Log("Hit");
        StartCoroutine(GetHit((transform.position - enemy).normalized * 300));
    }
    IEnumerator GetHit(Vector3 hitForce)
    {
        hitForce = Vector3.Scale(hitForce, new(1,0,1));
        Debug.Log("Entra GetHit");
        yield return new WaitForSecondsRealtime(.5f);
        //rb.AddForce(hitForce);
        rb.velocity = hitForce;
        Debug.Log("Golpeado por: " + hitForce);
    }

    public void PickUpAdmin(GameObject obj, int position)
    {
        if (obj.TryGetComponent(out IRecollectable iRec) && inventory.Count < 4)
        {
            var prefab = iRec.Recollect();
            inventory.Add(Instantiate(prefab, inventorySlots[position].transform));
            inventory[^1].GetComponentInChildren<MeshRenderer>().material.DisableKeyword("_EMISSION");
            inventory[^1].name = inventory[^1].name[0..^7];
        }
    }
    public void Save()
    {
        PlayerPrefs.SetString("Position", $"{transform.position.x}/{transform.position.y}/{transform.position.z}");
        PlayerPrefs.SetString("Rotation", $"{transform.rotation.x}/{transform.rotation.y}/{transform.rotation.z}/{transform.rotation.w}");

        foreach (var item in inventory)
        {
            PlayerPrefs.SetFloat(item.name, inventory.IndexOf(item));
        }
    }

    // Input System
    public void Movement(InputAction.CallbackContext ctx)
    {
        movement = ctx.ReadValue<Vector2>();
    }
    public void Jump(InputAction.CallbackContext ctx)
    {
        if (onGround && anim.GetFloat("Attacking") == 0 && jumpDelay == 0 && !anim.GetBool("Dance") && !anim.GetBool("Death"))
        {
            jumpDelay = 15;
            anim.SetTrigger("Jump");
        }
    }
    public void Run(InputAction.CallbackContext ctx)
    {
        anim.SetBool("Running", ctx.performed);
    }
    public void Crouch(InputAction.CallbackContext ctx)
    {
        anim.SetBool("Crouching", ctx.performed);
    }
    public void Attack(InputAction.CallbackContext ctx)
    {
        if (jumpDelay < 8 && anim.GetFloat("Attacking") == 0) anim.SetFloat("Attacking", 17);
    }
    public void Dance(InputAction.CallbackContext ctx)
    {
        if(!anim.GetBool("Death"))
        {
            anim.SetBool("Dance", ctx.performed);
            if (ctx.started)
            {
                SoundManager.instance.Play("DaleZelda");
                SoundManager.instance.ChangeVolume("OST", .1f);
            }
            if (ctx.canceled)
            {
                SoundManager.instance.Stop("DaleZelda");
                SoundManager.instance.ChangeVolume("OST", .5f);

            }
        }
    }
    public void Death(InputAction.CallbackContext ctx)
    {
        if(!anim.GetBool("Death")) anim.SetBool("Death", ctx.performed);
    }
    public void Aim(InputAction.CallbackContext ctx)
    {
        anim.SetBool("Aiming", ctx.performed);
    }
    public void ResetData(InputAction.CallbackContext ctx)
    {
        PlayerPrefs.DeleteAll();
        GameManager.CloseGame();
    }
    public void Interact(InputAction.CallbackContext ctx)
    {
        if(ctx.ReadValue<float>() == 1 && interactive != null && interactive.TryGetComponent(out IInteractive iInt) && interactCD == 0)
        {
            iInt.Interact(this);
            interactCD = 10;

            if (interactive.TryGetComponent(out IRecollectable iRec) && inventory.Count < 4)
            {
                var prefab = iRec.Recollect();
                inventory.Add(Instantiate(prefab, inventorySlots[inventory.Count].transform));
                //inventory[^1].GetComponentInChildren<MeshRenderer>().material.DisableKeyword("_EMISSION");
                inventory[^1].name = inventory[^1].name[0..^7];
                SoundManager.instance.Play("PickUp");
            }
        }
    }
}
